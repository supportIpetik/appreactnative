import React, { useState } from 'react';
import { View, StyleSheet, TouchableOpacity, Alert } from 'react-native';
import { Text } from 'react-native-paper';
import Background from '../components/Background';
import Logo from '../components/Logo';
import Header from '../components/Header';
import Button from '../components/Button';
import TextInput from '../components/TextInput';
import BackButton from '../components/BackButton';
import { theme } from '../core/theme';
import { emailValidator } from '../helpers/emailValidator';
import { passwordValidator } from '../helpers/passwordValidator';
import { nameValidator } from '../helpers/nameValidator';

export default function RegisterScreen({ navigation }) {
  const [dpi, setDpi] = useState({ value: '', error: '' });
  const [nombre, setNombre] = useState({ value: '', error: '' });
  const [apellido, setApellido] = useState({ value: '', error: '' });
  const [email, setEmail] = useState({ value: '', error: '' });
  const [password, setPassword] = useState({ value: '', error: '' });
  const [confirmPassword, setConfirmPassword] = useState({ value: '', error: '' });

  const onSignUpPressed = async () => {
    const nombreError = nameValidator(nombre.value);
    const emailError = emailValidator(email.value);
    const passwordError = passwordValidator(password.value);
    let confirmPasswordError = '';

    if (password.value !== confirmPassword.value) {
      confirmPasswordError = 'Las contraseñas no coinciden';
    }

    if (emailError || passwordError || nombreError || confirmPasswordError) {
      setNombre({ ...nombre, error: nombreError });
      setEmail({ ...email, error: emailError });
      setPassword({ ...password, error: passwordError });
      setConfirmPassword({ ...confirmPassword, error: confirmPasswordError });
      return;
    }

    const requestData = {
      dpi: dpi.value,
      nombre: nombre.value,
      apellido: apellido.value,
      email: email.value,
      password: password.value,
    };

    try {
      const response = await fetch('https://safemebk-production.up.railway.app/registro', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestData),
      });

      const result = await response.json();

      if (response.ok) {
        Alert.alert('Éxito', 'Registro exitoso');
        navigation.reset({
          index: 0,
          routes: [{ name: 'Dashboard' }],
        });
      } else {
        Alert.alert('Error', result.message || 'Error al registrar el reporte');
      }
    } catch (error) {
      console.error('Error al enviar los datos:', error);
      Alert.alert('Error', 'No se pudo conectar con el servidor');
    }
  };

  return (
    <Background>
      <BackButton goBack={navigation.goBack} />
      <Logo />
      <Header>Create Account</Header>

      <TextInput
        label="Dpi"
        returnKeyType="next"
        value={dpi.value}
        onChangeText={(text) => setDpi({ value: text, error: '' })}
        error={!!dpi.error}
        errorText={dpi.error}
      />
      <TextInput
        label="Nombre"
        returnKeyType="next"
        value={nombre.value}
        onChangeText={(text) => setNombre({ value: text, error: '' })}
        error={!!nombre.error}
        errorText={nombre.error}
      />
      <TextInput
        label="Apellido"
        returnKeyType="next"
        value={apellido.value}
        onChangeText={(text) => setApellido({ value: text, error: '' })}
        error={!!apellido.error}
        errorText={apellido.error}
      />
      <TextInput
        label="Email"
        returnKeyType="next"
        value={email.value}
        onChangeText={(text) => setEmail({ value: text, error: '' })}
        error={!!email.error}
        errorText={email.error}
        autoCapitalize="none"
        autoCompleteType="email"
        textContentType="emailAddress"
        keyboardType="email-address"
      />
      <TextInput
        label="Password"
        returnKeyType="done"
        value={password.value}
        onChangeText={(text) => setPassword({ value: text, error: '' })}
        error={!!password.error}
        errorText={password.error}
        secureTextEntry
      />
      <TextInput
        label="Confirmar Password"
        returnKeyType="done"
        value={confirmPassword.value}
        onChangeText={(text) => setConfirmPassword({ value: text, error: '' })}
        error={!!confirmPassword.error}
        errorText={confirmPassword.error}
        secureTextEntry
      />

      <Button
        mode="contained"
        onPress={onSignUpPressed}
        style={{ marginTop: 24 }}
      >
        Sign Up
      </Button>
      <View style={styles.row}>
        <Text>Ya tienes cuenta? </Text>
        <TouchableOpacity onPress={() => navigation.replace('LoginScreen')}>
          <Text style={styles.link}>Login</Text>
        </TouchableOpacity>
      </View>
    </Background>
  );
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  link: {
    fontWeight: 'bold',
    color: theme.colors.primary,
  },
});
