import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet, View, Alert } from 'react-native';
import { Text } from 'react-native-paper';
import Background from '../components/Background';
import Logo from '../components/Logo';
import Header from '../components/Header';
import Button from '../components/Button';
import TextInput from '../components/TextInput';
import BackButton from '../components/BackButton';
import { theme } from '../core/theme';

export default function LoginScreen({ navigation }) {
  const [password, setPassword] = useState({ value: '', error: '' });
  const [cui, setCui] = useState({ value: '', error: '' });

  const onLoginPressed = async () => {
    // Valida el CUI antes de enviar la solicitud
    if (!cui.value) {
      setCui({ ...cui, error: 'CUI no ingresado' });
      return;
    }
    if (!password.value) {
      setCui({ ...cui, error: 'Contraseña no ingresada' });
      return;
    }
    try {
      const response = await fetch('https://safemebk-production.up.railway.app/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ dpi: cui.value, password: password.value  }), // Envía el CUI como dpi
      });

      const data = await response.json();

      if (response.ok) {
        // Login exitoso, navega al Dashboard
        navigation.reset({
          index: 0,
          routes: [{ name: 'Dashboard' }],
        });
      } else {
        // Si la respuesta no es exitosa, muestra un mensaje de error
        Alert.alert('Error de inicio de sesión', data.message || 'Usuario no encontrado');
      }
    } catch (error) {
      console.error('Error al iniciar sesión:', error);
      Alert.alert('Error', 'No se pudo conectar con el servidor.');
    }
  };

  return (
    <Background>
      <BackButton goBack={navigation.goBack} />
      <Logo />
      <Header>Bienvenido devuelta</Header>
      <TextInput
        label="CUI"
        returnKeyType="next"
        value={cui.value}
        onChangeText={(text) => setCui({ value: text, error: '' })}
        error={!!cui.error}
        errorText={cui.error}
      />
      <TextInput
        label="Password"
        returnKeyType="done"
        value={password.value}
        onChangeText={(text) => setPassword({ value: text, error: '' })}
        error={!!password.error}
        errorText={password.error}
        secureTextEntry
      />
      <Button mode="contained" onPress={onLoginPressed}>
        Login
      </Button>
      <View style={styles.row}>
        <Text>Don’t have an account? </Text>
        <TouchableOpacity onPress={() => navigation.replace('RegisterScreen')}>
          <Text style={styles.link}>Sign up</Text>
        </TouchableOpacity>
      </View>
    </Background>
  );
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  link: {
    fontWeight: 'bold',
    color: theme.colors.primary,
  },
});
