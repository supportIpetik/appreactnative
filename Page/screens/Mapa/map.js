/* import { StatusBar } from 'expo-status-bar';
import React, { useEffect, useState } from 'react';
import { StyleSheet, View, Text, TextInput } from 'react-native';
import MapView, { Marker } from 'react-native-maps';
import * as Location from 'expo-location';
import axios from 'axios'; // Importa axios

export default function Map({ navigation }) {
  const [origin, setOrigin] = useState(null);
  const [searchText, setSearchText] = useState('');

  useEffect(() => {
    (async () => {
      // Solicitar permiso de ubicación
      const { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== 'granted') {
        console.error('Permiso de ubicación no concedido');
        return;
      }

      // Obtener la ubicación actual
      const currentLocation = await Location.getCurrentPositionAsync({});
      setOrigin(currentLocation.coords);
    })();
  }, []);

  // Función para manejar cambios en el cuadro de búsqueda
  const handleSearchTextChange = async (text) => {
    setSearchText(text);

    // Verificar si se presionó "Enter"
    if (text.endsWith('\n')) {
      const apiKey = 'TU_API_KEY_DE_GOOGLE_PLACES'; // Reemplaza con tu API Key
      const query = text.trim(); // Eliminar espacios en blanco
      try {
        const response = await axios.get(
          `https://maps.googleapis.com/maps/api/geocode/json?address=${query}&key=${apiKey}`
        );

        if (response.data.results.length > 0) {
          // Obtén las coordenadas de ubicación
          const location = response.data.results[0].geometry.location;

          // Actualiza el mapa con la nueva ubicación
          setOrigin(location);
        } else {
          console.error('No se encontró ninguna ubicación.');
        }
      } catch (error) {
        console.error('Error al realizar la búsqueda de ubicación:', error);
      }
    }
  };

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.searchInput}
        placeholder="Buscar ubicación y presionar Enter"
        value={searchText}
        onChangeText={handleSearchTextChange}
        onSubmitEditing={() => {
          // Limpia el cuadro de búsqueda cuando se presiona Enter
          setSearchText('');
        }}
      />
      {origin && (
        <MapView
          style={styles.map}
          initialRegion={{
            latitude: origin.latitude,
            longitude: origin.longitude,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
        >
          <Marker
            coordinate={{
              latitude: origin.latitude,
              longitude: origin.longitude,
            }}
            title="Mi ubicación"
          />
        </MapView>
      )}
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  map: {
    flex: 1,
  },
  searchInput: {
    height: 40,
    borderWidth: 1,
    borderColor: 'gray',
    margin: 10,
    paddingLeft: 10,
  },
});
 */