import React from "react";
import { View, Image, TouchableOpacity, Text, StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/native";

export default function Menu({ onMapPress, onReportPress, onExitPress }) {
  const navigation = useNavigation();
  const handleReportPress = () => {
    // Navega a la pantalla ReportScreen dentro de la carpeta ReportScreen
    navigation.navigate("ReportStack", {
      /* aquí puedes pasar parámetros si es necesario */
    });
  };
  const handleOnMap = () => {
    // Navega a la pantalla ReportScreen dentro de la carpeta ReportScreen
    navigation.navigate("Map", {
      /* aquí puedes pasar parámetros si es necesario */
    });
  };
  const regresarAlLogin = () => {
    // Navega a la pantalla ReportScreen dentro de la carpeta ReportScreen
    navigation.navigate("LoginScreen", {
      /* aquí puedes pasar parámetros si es necesario */
    });
  };
  return (
    <View style={styles.container}>
      {/* Contenedor para la imagen de arriba y los botones */}
      <View style={styles.imageBottomContainer}>
      <Image
          source={require("../assets/segurty.png")}
          style={[styles.imageTop]}
          resizeMode="contain"
        />
      </View>
      <View style={styles.contentContainer}>
       

        {/* Contenedor para botones */}
        <View style={styles.buttonContainer}>
          {/* Botones centrados */}
          <TouchableOpacity style={styles.button} onPress={() => Linking.openURL('https://www.google.com/maps/d/u/0/edit?mid=1aysrZq1DV1ReA5eiGiAKXpU1Le7r1LY&usp=sharing')}>
            <Text style={styles.buttonText}>Mapa</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button} onPress={handleReportPress}>
            <Text style={styles.buttonText}>Reporte</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button} onPress={regresarAlLogin}>
            <Text style={styles.buttonText}>Salir</Text>
          </TouchableOpacity>
        </View>
      </View>

      {/* Imagen de abajo */}
      <View style={styles.imageBottomContainer}>
        <Image
          source={require("../assets/megafono.png")}
          style={[styles.imageBottom]}
          resizeMode="contain"
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
  },
  contentContainer: {
    alignItems: "center",
  },
  imageTop: {
    marginBottom: 140,
    width: 200, // ajusta el ancho según tus necesidades
    height: 100, // ajusta la altura según tus necesidades
  },
  buttonContainer: {
    justifyContent: "center", // Centra verticalmente los botones
    alignItems: "center", 
  },
  button: {
    backgroundColor: "transparent",
    padding: 20,
    marginVertical: 10,
    width: 200,
    alignItems: "center",
    borderRadius: 10,
    borderWidth: 4, // Ancho del borde
    borderColor: "#083454", // Color del borde
  },
  buttonText: {
    fontSize: 18,
  },
  imageBottomContainer: {
    justifyContent: "flex-end",
    alignItems: "center",
    flex: 1,
  },
  imageBottom: {
    marginLeft:50,
    width: 150,
    height: 150,
  },
});
