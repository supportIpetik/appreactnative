import React, { useState } from 'react';
import { View, TextInput, TouchableOpacity, Text, StyleSheet, Alert } from 'react-native';
import * as Location from 'expo-location';
import axios from 'axios';
import { useNavigation } from '@react-navigation/native';

export default function QuestionsScreen({ route }) {
  const navigation = useNavigation();
  const { category } = route.params;
  const [dateTime, setDateTime] = useState('');
  const [location, setLocation] = useState('');
  const [hasMinors, setHasMinors] = useState(false);
  const [report, setReport] = useState('');

  const handleSend = async () => {
    if (!dateTime || !location || !report) {
      Alert.alert('Error', 'Por favor, completa todos los campos.');
      return;
    }

    try {
      const response = await fetch('https://safemebk-production.up.railway.app/registro_form', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          fecha: dateTime,
          ubicacion: location,
          menores: hasMinors,
          informe: report,
          category: category,
        }),
      });

      const data = await response.json();

      if (response.ok) {
        Alert.alert('Enviado', 'Reporte enviado exitosamente.');
        navigation.goBack();
      } else {
        Alert.alert('Error', data.message || 'No se pudo enviar el reporte.');
      }
    } catch (error) {
      console.error('Error al enviar el reporte:', error);
      Alert.alert('Error', 'No se pudo conectar con el servidor.');
    }
  };

  const getCurrentLocation = async () => {
    // Solicitar permiso de ubicación
    let { status } = await Location.requestForegroundPermissionsAsync();
    if (status !== 'granted') {
      Alert.alert("Permiso denegado", "No se puede obtener la ubicación sin permisos.");
      return;
    }

    // Obtener la ubicación actual
    let position = await Location.getCurrentPositionAsync({});
    const { latitude, longitude } = position.coords;

    // Llamada a OpenCage para obtener la dirección
    try {
      const response = await axios.get(`https://api.opencagedata.com/geocode/v1/json?q=${latitude}+${longitude}&key=eb7718cd546c4beda19a3ad96c3615b1`);
      console.log("Respuesta de OpenCage:", response.data); // Agregar un log de la respuesta
    
      if (response.data.results && response.data.results.length > 0) {
        const fullAddress = response.data.results[0].formatted;
        setLocation(fullAddress); // Actualiza el estado con la dirección obtenida
      } else {
        console.error("Error: Respuesta de OpenCage sin resultados:", response.data);
        Alert.alert("Error", "No se pudo obtener la dirección. Respuesta sin resultados.");
      }
    } catch (error) {
      console.error("Error en la solicitud de OpenCage:", error); // Log del error completo
      Alert.alert("Error", "No se pudo convertir las coordenadas.");
    }    
  };

  return (
    <View style={styles.container}>
      <Text style={styles.categoryText}>Categoría: {category}</Text>
      <TextInput
        style={styles.input}
        placeholder="DD-MM-AAAA"
        onChangeText={(text) => setDateTime(text)}
        value={dateTime}
      />
      <TouchableOpacity style={styles.button} onPress={getCurrentLocation}>
        <Text style={styles.buttonText}>Mandar ubicación actual</Text>
      </TouchableOpacity>
      <Text style={styles.locationText}>Ubicación: {location}</Text>
      <TextInput
        style={styles.input}
        placeholder="Informe"
        onChangeText={(text) => setReport(text)}
        value={report}
      />
      <TouchableOpacity style={styles.button} onPress={() => setHasMinors(!hasMinors)}>
        <Text style={styles.buttonText}>
          {hasMinors ? 'Hay menores de edad: Sí' : 'Hay menores de edad: No'}
        </Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.sendButton} onPress={handleSend}>
        <Text style={styles.buttonText}>Enviar</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.cancelButton} onPress={() => navigation.goBack()}>
        <Text style={styles.buttonText}>Cancelar</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  categoryText: {
    fontSize: 20,
    marginBottom: 20,
  },
  input: {
    borderColor: 'gray',
    borderWidth: 1,
    width: 300,
    padding: 10,
    marginBottom: 20,
    borderRadius: 10,
  },
  button: {
    backgroundColor: 'lightblue',
    padding: 10,
    width: 300,
    alignItems: 'center',
    borderRadius: 10,
  },
  sendButton: {
    backgroundColor: 'lightgreen',
    padding: 10,
    width: 200,
    marginTop: 20,
    alignItems: 'center',
    borderRadius: 10,
  },
  cancelButton: {
    backgroundColor: 'lightcoral',
    padding: 10,
    width: 200,
    marginTop: 10,
    alignItems: 'center',
    borderRadius: 10,
  },
  buttonText: {
    fontSize: 18,
  },
  locationText: {
    fontSize: 16,
    marginVertical: 10,
  },
});
