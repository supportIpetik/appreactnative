import React from "react";
import {
  View,
  TouchableOpacity,
  Text,
  StyleSheet,
  Button,
  Alert,
  Image,
} from "react-native";

export default function ReportScreen({ navigation }) {
  const handleCategoryPress = (category) => {
    // Puedes realizar acciones específicas según la categoría seleccionada aquí
    // Por ejemplo, puedes navegar a la pantalla de preguntas con parámetros de categoría
    navigation.navigate("QuestionsScreen", { category });
  };

  return (
    <View style={styles.container}>
      <View style={styles.imageBottomContainer}>
        <Image
          source={require("../../assets/segurty.png")}
          style={[styles.imageTop]}
          resizeMode="contain"
        />
      </View>
      <View style={styles.contentContainer}>
      <View style={styles.buttonContainer}>
        <TouchableOpacity
          style={styles.button}
          onPress={() => handleCategoryPress("Homicidio")}
        >
          <Text style={styles.buttonText}>Homicidio</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => handleCategoryPress("Abuso contra la mujer")}
        >
          <Text style={styles.buttonText}>Abuso contra la mujer</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => handleCategoryPress("Estructuras dañadas")}
        >
          <Text style={styles.buttonText}>Estructuras dañadas</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.backButton}
          onPress={() => navigation.goBack()}
        >
          <Text style={styles.buttonText}>Regresar al menú principal</Text>
        </TouchableOpacity>
      </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  contentContainer: {
    alignItems: "center",
  },
  imageTop: {
    marginBottom: 100,
    width: 200, // ajusta el ancho según tus necesidades
    height: 100, // ajusta la altura según tus necesidades
  },
  buttonContainer: {
    justifyContent: "center", // Centra verticalmente los botones
    alignItems: "center", 
  },
  button: {
    backgroundColor: "lightblue",
    padding: 10,
    marginVertical: 10,
    width: 250,
    alignItems: "center",
    borderRadius: 10,
  },
  imageBottomContainer: {
    alignItems: "center",
  },
  backButton: {
    backgroundColor: "lightcoral",
    padding: 10,
    marginVertical: 10,
    width: 200,
    alignItems: "center",
    borderRadius: 10,
  },
  buttonText: {
    fontSize: 18,
    textAlign: "center",
  },
});
