// LoginScreen.js

import React, { useState } from 'react';
import { View, Text, TextInput, Button, Alert } from 'react-native';
const LoginScreen = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleLogin = () => {
    if (username === 'admin' && password === '1234') {
      Alert.alert('Éxito', 'Has iniciado sesión correctamente');
    } else {
      Alert.alert('Error', 'Usuario o contraseña incorrectos');
    }
  };

  const handleRegister = () => {
    // Aquí puedes poner la lógica para manejar el registro de nuevos usuarios
    Alert.alert('Registrar', 'Botón de registro presionado');
  };

  return (
    <View style={{ flex: 1, justifyContent: 'center', padding: 20}}>
      <Text style={{ fontSize: 24, marginBottom: 20, color: 'black', textAlign:'center' }}>Iniciar sesión</Text>
      <TextInput
        placeholder="Usuario"
        placeholderTextColor="black"
        value={username}
        onChangeText={setUsername}
        style={{ borderWidth: 1, marginBottom: 20, padding: 10, color: 'black' }}
      />
      <TextInput
        placeholder="Contraseña"
        placeholderTextColor="black"
        value={password}
        onChangeText={setPassword}
        secureTextEntry
        style={{ borderWidth: 1, marginBottom: 20, padding: 10, color: 'black' }}
      />
      <Button title="Iniciar sesión" onPress={handleLogin} />
      <View style={{ marginTop: 10 }}>
        <Button title="Registrar" onPress={handleRegister} color="#34eb4f" />
      </View>
    </View>
  );
};

export default LoginScreen;
