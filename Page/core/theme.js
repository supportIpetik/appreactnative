import { DefaultTheme } from 'react-native-paper'

export const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    text: '#000000',
    primary: '#414757',
    secondary: '#FAFAFA',
    error: '#f13a59',
  },
}
