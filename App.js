import React from 'react'
import { Provider } from 'react-native-paper'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { theme } from './Page/core/theme'
import {
  StartScreen,
  LoginScreen,
  RegisterScreen,
  ResetPasswordScreen,
  Dashboard,
} from './Page/screens'
import ReportStackScreen from './Page/screens/reportes/ReportScreen'; // Asegúrate de importar la pantalla de ReportScreen correctamente
import QuestionsScreen from './Page/screens/reportes/QuestionsScreen'; 
import Map from './Page/screens/Mapa/map'; 
const Stack = createStackNavigator()

export default function App() {
  return (
    <Provider theme={theme}  style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#083454'  }}>
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName="StartScreen"
          screenOptions={{
            headerShown: false,
          }}
        >
          <Stack.Screen name="StartScreen" component={StartScreen} />
          <Stack.Screen name="LoginScreen" component={LoginScreen} />
          <Stack.Screen name="RegisterScreen" component={RegisterScreen} />
          <Stack.Screen name="Dashboard" component={Dashboard} />
          <Stack.Screen name="ReportStack" component={ReportStackScreen} />
          <Stack.Screen name="QuestionsScreen" component={QuestionsScreen} />
          <Stack.Screen
            name="ResetPasswordScreen"
            component={ResetPasswordScreen}
          />
          <Stack.Screen name="Map" component={Map} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  )
}
