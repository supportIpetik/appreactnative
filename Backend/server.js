const express = require('express');
const app = express();
const port = 3000;

// Importa las rutas de tu API
const registroRouter = require('./API/registroRouter');
const loginAPI = require('./API/loginAPi');

// Conexión a la base de datos
const mysql = require('mysql2');
const connection = mysql.createConnection({
  host: '127.0.0.1',
  user: 'root',
  password: '209001',
  database: 'safeme',
  port: 3316
});

// Middleware para analizar el cuerpo de la solicitud
app.use(express.json());

// Usa las rutas en tu servidor y pasa la conexión a la base de datos
app.use('/', registroRouter(connection));
app.use('/', loginAPI(connection));

app.listen(port, () => {
  console.log(`Servidor en ejecución en el puerto ${port}`);
});
