const mysql = require('mysql2');

// Configura la conexión a la base de datos
const connection = mysql.createConnection({
  host: '127.0.0.1',
  user: 'root',
  password: '209001',
  database: 'safeme',
  port: 3316
});

// Conecta a la base de datos
connection.connect((err) => {
  if (err) {
    console.error('Error al conectar a la base de datos:', err);
    return;
  }
  console.log('Conexión a la base de datos establecida');

  // Ahora que la conexión está establecida, ejecuta la consulta de inserción
  const queryString = 'INSERT INTO usuarios (nombre, dpi) VALUES (?, ?)';
  const valores = ['Johan', 3242540751003];

  connection.query(queryString, valores, (err, results) => {
    if (err) {
      console.error('Error al insertar el usuario en la base de datos:', err);
      return;
    }

    console.log('Usuario registrado correctamente');

    // Cierra la conexión a la base de datos después de completar la inserción
    connection.end((err) => {
      if (err) {
        console.error('Error al cerrar la conexión:', err);
        return;
      }
      console.log('Conexión a la base de datos cerrada');
    });
  });
});
